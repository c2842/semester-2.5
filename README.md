# Semester 2.5

Things to do over the summer


## Classbot
The overall goal of this bot is to support teh class in a practical way as well as provide a group project.  
Its designed top eb fairly easy to add extra modules to extend the functionality.

[Repo][Classbot]

### Tech

* Typescript (like java)
* Databases (sqlite)
* CI/CD (automation done right)
* Git (gud)

### Stuff to do

* Needs a rework on how reacts are handled.
* Needs to accomodate stuff on a per module basis (now we are different courses)
* Message ye 72hrs in advance of something due?
* Counting module
* Hans Mute module?

## Reading
Lots of books ye can read to improve yerselves

* [The Phoenix Project][Book-Phoenix]
* [The Rust (lang) Book][Book-Rust]
  * Rust is an interesting language that is a mix of high level and low level, with a strong focus on memory safety
* [Learn C][Book-C]
  * We will be using it next year
* [The Design of Everyday Things][Book-Design]
  * YOu dont have to draw inspiration from jsut computer science stuff


## Advent of Code
The [Advent of Code][AoC] is a yearly coding puzzle challenge.  
It starts off pretty easy and escalates, coupled a nice ~~wild~~ story.

You should have enough knowledge about reading files, parsing teh input and workign with that now (CS4222 Assignment 3).  
A reminder that we have a [Discord][AoC-Discord] channel.

## Contact
If you feel tehre is more to add here feel free to create a merge request or to contact me at ``Silver#5563``


[Classbot]: https://gitlab.com/c2842/misc/classbot
[Book-Phoenix]: https://www.google.com/search?q=the+phoenix+project
[Book-Rust]: https://doc.rust-lang.org/book/
[Book-C]: https://beej.us/guide/bgc/
[Book-Design]: https://www.google.com/search?q=the+design+of+everyday+things
[AoC]: https://adventofcode.com/
[AoC-Discord]: https://discord.com/channels/890607420055896085/915665123186704404
